const sequelize = require("../config/db");

module.exports = (sequelize, DataTypes) => {
    return sequelize.define("insula", {
        nume: DataTypes.STRING(20),
        suprafata: DataTypes.INTEGER,
    });
};