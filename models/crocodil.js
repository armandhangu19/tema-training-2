const sequelize = require("../config/db");

module.exports = (sequelize, DataTypes) => {
    return sequelize.define("crocodil", {
        nume: DataTypes.STRING,
        prenume: DataTypes.STRING,
        varsta: DataTypes.INTEGER
    });
};