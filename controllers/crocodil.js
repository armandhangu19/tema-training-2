const CrocodilDb = require("../models").Crocodil;
const InsulaDb = require("../models").Insula;

const controller = {
    getAllCrocodiles: async (req, res) => {
        CrocodilDb.findAll().then((crocodili) => {
            res.status(200).send(crocodili);
        }).catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server error!" });
        });
    },

    addCrocodile: async(req,res) => {
        const {idInsula, nume, prenume, varsta} = req.body;
        InsulaDb.findByPk(idInsula).then((insula) => {
            if(insula) {
                insula.createCrocodil({nume, prenume, varsta}).then((crocodil) => {
                    res.status(201).send(crocodil);
                }).catch((err) => {
                    console.log(err);
                    res.status(500).send({message: "Server error!"});
                });
            } else {
                res.status(404).send({message: "Insula not found!"});
            }
        }).catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server error!"});
        });
    },

    deleteCrocodile: async (req,res) => {
        const id = `${req.params.id}`;
        CrocodilDb.findByPk(id).then((crocodil) => {
            if(crocodil) {
                crocodil.destroy({
                //where: { id: req.params.id}
            }).then(() => {
                    res.status(201).send({message: "Crocodil eliminat!"});
                }).catch((err) => {
                    console.log(err);
                    res.status(500).send({message: "Server error!"});
                });
            } else {
                res.status(404).send({message: "Crocodil not found!"});
            }  
        }).catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server error!"});
        });
    },

    updateName: async (req,res) => {
        const id = `${req.params.id}`;
        const {nume} = req.body;
        CrocodilDb.findByPk(id).then((crocodil) => {
            if(crocodil) {
                crocodil.update({
                    nume: nume
                }).then(() => {
                        res.status(201).send({message: "Crocodil cu nume actualizat"});
                    }).catch((err) => {
                        console.log(err);
                        res.status(500).send({message: "Server error!"});
                    });
            } else {
                res.status(404).send({message: "Crocodil not found!"});
            }
        }).catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server error!"});
        });
    },
};

module.exports = controller;
